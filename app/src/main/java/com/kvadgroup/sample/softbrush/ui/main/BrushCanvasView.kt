package com.kvadgroup.sample.softbrush.ui.main


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import kotlin.math.abs

private const val DEFAULT_STROKE_WIDTH = 20f

class BrushCanvasView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {


    // Holds the current path
    private var path = Path()

    //only for current path (while user move his finger on screen)
    private lateinit var currentCanvas: Canvas
    private lateinit var currentBitmap: Bitmap

    // holds all drawings
    private lateinit var drawingCanvas: Canvas
    private lateinit var drawingBitmap: Bitmap

    private val paint = Paint().apply {
        color = Color.RED
        isAntiAlias = true
        isDither = true
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
        strokeCap = Paint.Cap.ROUND
    }

    private val touchTolerance = ViewConfiguration.get(context).scaledTouchSlop

    private var currentX = 0f
    private var currentY = 0f

    private var motionTouchEventX = 0f
    private var motionTouchEventY = 0f

    init {
        setBrushWidth(DEFAULT_STROKE_WIDTH)
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)

        if (::currentBitmap.isInitialized) currentBitmap.recycle()
        currentBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        currentCanvas = Canvas(currentBitmap)

        if (::drawingBitmap.isInitialized) drawingBitmap.recycle()
        drawingBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        drawingCanvas = Canvas(drawingBitmap)
        drawingCanvas.drawColor(Color.WHITE) //bg for exported image

    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawBitmap(drawingBitmap, 0f, 0f, null)
        canvas.drawBitmap(currentBitmap, 0f, 0f, null)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        motionTouchEventX = event.x
        motionTouchEventY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> onTouchActionStart()
            MotionEvent.ACTION_MOVE -> onTouchActionMove()
            MotionEvent.ACTION_UP -> onTouchActionUp()
        }
        return true
    }

    private fun onTouchActionStart() {
        path.reset()
        path.moveTo(motionTouchEventX, motionTouchEventY)
        currentX = motionTouchEventX
        currentY = motionTouchEventY

        // for single tap without movement
        currentCanvas.drawPoint(motionTouchEventX, motionTouchEventY, paint)
    }

    private fun onTouchActionMove() {
        val dx = abs(motionTouchEventX - currentX)
        val dy = abs(motionTouchEventY - currentY)

        // optimization: do not draw for small movement
        if (dx >= touchTolerance || dy >= touchTolerance) {
            path.quadTo(
                currentX,
                currentY,
                (motionTouchEventX + currentX) / 2,
                (motionTouchEventY + currentY) / 2
            )
            currentX = motionTouchEventX
            currentY = motionTouchEventY

            currentCanvas.clear()
            currentCanvas.drawPath(path, paint)

            invalidate()
        }
    }

    /**
     * "save" current drawing (from ACTION_DOWN to ACTION_UP) to drawingCanvas and clear currentCanvas.
     * Now drawingCanvas contains whole painting and can be used for saving to file
     */
    private fun onTouchActionUp() {
        drawingCanvas.drawBitmap(currentBitmap, 0f, 0f, null)
        currentCanvas.clear()
        invalidate()
    }

    fun clear() {
        drawingCanvas.drawColor(Color.WHITE)
        currentCanvas.clear()
        invalidate()
    }

    fun setBrushWidth(width: Float) {
        paint.strokeWidth = width
        paint.maskFilter = BlurMaskFilter(10 + width / 5, BlurMaskFilter.Blur.NORMAL)
    }

    fun setBrushColor(color: Int) {
        paint.color = color
    }

    private fun Canvas.clear() {
        this.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
    }

    fun getBitmap(): Bitmap = drawingBitmap
}