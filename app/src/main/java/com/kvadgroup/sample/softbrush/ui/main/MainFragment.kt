package com.kvadgroup.sample.softbrush.ui.main

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import codes.side.andcolorpicker.converter.setFromColorInt
import codes.side.andcolorpicker.converter.toColorInt
import codes.side.andcolorpicker.group.PickerGroup
import codes.side.andcolorpicker.group.registerPickers
import codes.side.andcolorpicker.hsl.HSLColorPickerSeekBar
import codes.side.andcolorpicker.model.IntegerHSLColor
import codes.side.andcolorpicker.view.picker.ColorSeekBar
import com.kvadgroup.sample.softbrush.R
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.FileDescriptor
import java.io.FileOutputStream
import java.io.IOException


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()

        // Request code for creating a file.
        const val REQUEST_CODE_CREATE_FILE = 1
        const val MIN_BRUSH_SIZE = 10f
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    private val onBrushSizeSeekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            //no-op
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
            //no-op
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            brush_canvas.setBrushWidth(MIN_BRUSH_SIZE + (seekBar?.progress ?: 0))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureColorPicker()
        button_clear.setOnClickListener { brush_canvas.clear() }
        button_save.setOnClickListener { createFile() }
        sizeSeekBar.setOnSeekBarChangeListener(onBrushSizeSeekBarChangeListener)
        sizeSeekBar.progress = 10
    }

    private fun configureColorPicker() {
        val defaultColor = IntegerHSLColor().apply {
            setFromColorInt(Color.GREEN)
        }

        PickerGroup<IntegerHSLColor>().apply {
            registerPickers(hueSeekBar, alphaSeekBar)
            addListener(listener = object :
                HSLColorPickerSeekBar.DefaultOnColorPickListener() {
                override fun onColorChanged(
                    picker: ColorSeekBar<IntegerHSLColor>,
                    color: IntegerHSLColor,
                    value: Int
                ) {
                    brush_canvas.setBrushColor(color.toColorInt())
                }
            })
            setColor(defaultColor)
        }
    }

    private fun createFile() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "image/png"
            putExtra(Intent.EXTRA_TITLE, "sample.png")
        }
        startActivityForResult(intent, REQUEST_CODE_CREATE_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (requestCode == REQUEST_CODE_CREATE_FILE && resultCode == RESULT_OK) {
            resultData?.data?.also { uri ->
                activity?.contentResolver
                    ?.openFileDescriptor(uri, "w")
                    ?.fileDescriptor
                    ?.let { saveToFile(brush_canvas.getBitmap(), it) }
            }
        }
    }

    private fun saveToFile(bitmap: Bitmap, file: FileDescriptor) {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val out = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                out.flush()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            withContext(Dispatchers.Main) {
                Toast.makeText(context, "Image saved", Toast.LENGTH_SHORT).show()
            }
        }
    }

}