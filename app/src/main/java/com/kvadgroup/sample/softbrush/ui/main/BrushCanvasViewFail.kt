package com.kvadgroup.sample.softbrush.ui.main


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.kvadgroup.sample.softbrush.R

/**
 * Not useful implementation
 */
class BrushCanvasViewFail @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {

    private var brushSize: Float = 0f
    private lateinit var brushDrawable: Drawable
    private lateinit var brushBitmap: Bitmap

    private val defaultColor = ResourcesCompat.getColor(resources, R.color.colorPaintDefault, null)
    private val backgroundColor = ResourcesCompat.getColor(resources, R.color.colorBackground, null)

    private lateinit var drawingCanvas: Canvas
    private lateinit var drawingBitmap: Bitmap

    private val paint = Paint().apply {
        isAntiAlias = true
        isDither = true
        colorFilter = PorterDuffColorFilter(defaultColor, PorterDuff.Mode.SRC_IN)
    }

    private var currentX = 0f
    private var currentY = 0f

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)

        if (::drawingBitmap.isInitialized) drawingBitmap.recycle()

        setBrushWidth(50f)

        drawingBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        drawingCanvas = Canvas(drawingBitmap)
        drawingCanvas.drawColor(backgroundColor)

    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawBitmap(drawingBitmap, 0f, 0f, null)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        currentX = event.x
        currentY = event.y

        when (event.action) {
            MotionEvent.ACTION_MOVE -> onTouchActionMove()
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_UP -> {
            }
        }
        return true
    }

    private fun onTouchActionMove() {
        drawingCanvas.drawBitmap(
            brushBitmap,
            currentX - brushSize / 2,
            currentY - brushSize / 2,
            paint
        )
        invalidate()
    }

    fun setBrushWidth(size: Float) {
        brushSize = size
        if (!::brushDrawable.isInitialized) {
            brushDrawable = resources.getDrawable(R.drawable.soft_brush, null)
        }
        brushBitmap = (brushDrawable as BitmapDrawable).toBitmap(size.toInt(), size.toInt())
    }

    fun clear() {
        drawingCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        invalidate()
    }

    fun setBrushColor(color: Int) {
        paint.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
    }

    fun getBitmap(): Bitmap = drawingBitmap
}